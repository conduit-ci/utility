# conduit

## Description
A set of ci/cd jobs to be imported by other pipelines

## Usage
TODO

## Support
TODO

## Roadmap
- Add renovate job
- Add docs jobs?
- add issue templates
- add mr templates
- Add grype job
- Add sboms? (syft)
- Add go build job
- Add go test job
- Add go lint job(?)
- Add go publish job
- Add docker/podman build
- Add docker/podman lint (hadolint)
- Add docker/podman publish
- Docker in docker shenanigans?

## Contributing
TODO


## Project status
Under active development
