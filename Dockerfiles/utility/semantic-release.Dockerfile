FROM node:20-alpine

RUN apk update && apk add git ca-certificates

RUN npm install -g semantic-release @semantic-release/gitlab @semantic-release/git
